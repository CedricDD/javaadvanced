package oef8;

/**
 * Created by C�dric De Dycker on 11/10/2015.
 */
public class TextApp {
    public static void main(String[] args) {
        FlterBouwer fb =  new FlterBouwer();
        Text text = new Text("Hallo ik beeen eeeeen banaan baaaaaaaaaaaaaaaaaanan");

        System.out.println("*** Contains a ***");
        text.printFilteredWords(fb.bouwBevat("a"));
        System.out.println("*** Two check ***");
        text.printFilteredWords(fb.bouwDubbelCheck(fb.bouwLetterControle('e', 1), fb.bouwMinimumLengte(2)));

    }
}
