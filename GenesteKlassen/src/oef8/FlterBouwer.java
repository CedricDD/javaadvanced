package oef8;

/**
 * Created by C�dric De Dycker on 11/10/2015.
 */
public class FlterBouwer {

    public WordFilter bouwBevat(String tekst){
        return ((s) -> s.contains(tekst));
    }

    public WordFilter bouwMinimumLengte(int minLen){
        return (s -> s.length() >= minLen);
    }

    public WordFilter bouwLetterControle(char letter, int pos){
        return (s -> s.charAt(pos) == letter);
    }

    public WordFilter bouwDubbelCheck(WordFilter filter1, WordFilter filter2){
        return s -> (filter1.isValid(s) && filter2.isValid(s));
    }
}
