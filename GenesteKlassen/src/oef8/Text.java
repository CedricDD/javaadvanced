package oef8;

import oef5.NumberParcer;
import oef5.WordProcessor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by C�dric De Dycker on 11/10/2015.
 */
public class Text {
    private String sentence;

    public Text(String sentence) {
        this.sentence = sentence;
    }

    public void printFilteredWords(WordFilter filter){
        for(String w : sentence.split(" ")){
            if(filter.isValid(w)){
                System.out.println(w);
            }
        }
    }

    public void printProcessedWords(WordProcessor processor){
        for(String w : sentence.split(" ")){
            System.out.println(processor.process(w));
        }
    }

    public void printSum(NumberParcer n){
        List getallen = new ArrayList<>();
        for(String w : sentence.split(" ")){
            if(w.matches("\\d+")) getallen.add(n.parse(w));
        }
        System.out.println(Arrays.toString(getallen.toArray()));

    }

}
