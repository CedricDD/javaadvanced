package oef5;

/**
 * Created by C�dric De Dycker on 11/10/2015.
 */
public interface NumberParcer {
    public double parse(String s);
}
