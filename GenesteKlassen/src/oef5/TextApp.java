package oef5;

/**
 * Created by C�dric De Dycker on 11/10/2015.
 */
public class TextApp {
    public static void main(String[] args) {
        Text text = new Text("Ik ken echt niemand die beter is dan Cedric #GrootEgo 1 12 123 19181a 1aa1");
        System.out.println("*** Words containing e ***");
        text.printFilteredWords(s -> s.contains("e"));
        System.out.println("*** Long words ***");
        text.printFilteredWords(s -> s.length() > 4);
        System.out.println("*** Words starting with C ***");
        text.printFilteredWords(s -> s.startsWith("C"));

        //oef 4
        System.out.println("*** Words second letter e ***");
        text.printFilteredWords(s -> s.startsWith("e", 1));
        System.out.println("*** Words two letters e ***");
        text.printFilteredWords(s -> s.matches(".*e.*e.*"));

        //oef 5
        System.out.println("*** Quote ***");
        text.printProcessedWords(TextUtil::quote);
        System.out.println("*** Reverse ***");
        text.printProcessedWords((s) -> new StringBuilder(s).reverse().toString());


        //oef 5 vervolg
        System.out.println("*** Scrambled ***");
        text.printProcessedWords(s -> (new TextScrambler().scramble(s)).toLowerCase());
        System.out.println("*** Numbers ***");
        text.printSum(s -> Integer.parseInt(s));
    }
}
