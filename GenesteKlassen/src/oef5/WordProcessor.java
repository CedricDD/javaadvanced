package oef5;

/**
 * Created by C�dric De Dycker on 11/10/2015.
 */
@FunctionalInterface
public interface WordProcessor {

    public String process(String text);

}
