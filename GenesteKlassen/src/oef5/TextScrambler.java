package oef5;

/**
 * Created by C�dric De Dycker on 11/10/2015.
 */
public class TextScrambler {
    public String scramble(String s){
        String temp = s;
        if(temp.contains("a")) temp.replace("a", "@");
        if(temp.contains("e")) temp.replace("e", "@");
        if(temp.contains("l")) temp.replace("l", "1");
        if(temp.contains("o")) temp.replace("o", "0");
        return temp;
    }
}
