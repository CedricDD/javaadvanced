package oef6;

/**
 * Created by C�dric De Dycker on 11/10/2015.
 */
public class Main {
    public static void main(String[] args) {
        process(() -> "Hello World");
    }

    public static void process(Util u){
        System.out.println(u.test());
    }
}
