package oef1;

/**
 * Created by C�dric De Dycker on 9/10/2015.
 */
public class Main {

    public static void main(String[] args) {
        Musician m = new Musician();
        m.play();

        Musician.Instrument i = new Musician.Instrument();
        i.makeNoise();

        System.exit(0);
    }
}
