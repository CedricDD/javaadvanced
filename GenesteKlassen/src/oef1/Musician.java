package oef1;

/**
 * Created by C�dric De Dycker on 9/10/2015.
 */
public class Musician {

    public void play(){
        new Instrument().makeNoise();
    }

    //region Instrument
    public static class Instrument{
        public static void makeNoise(){
            System.out.println("Tu-du Tsssss");
        }
    }
    //endregion
}
