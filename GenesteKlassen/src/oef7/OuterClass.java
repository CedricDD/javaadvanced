package oef7;

/**
 * Created by C�dric De Dycker on 11/10/2015.
 */
public class OuterClass {

    public NestedClass make() {
        return new NestedClass();
    }

    private static class NestedClass implements Util{
        public String valuableData = "secret";

        public String getValuableData() {
            return valuableData;
        }
    }


}