package oef3;

/**
 * Created by C�dric De Dycker on 9/10/2015.
 */
public interface Instrument {

    void makeNoise(String s);
}
