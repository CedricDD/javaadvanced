package oef4;

/**
 * Created by C�dric De Dycker on 11/10/2015.
 */
public class TextApp {
    public static void main(String[] args) {
        Text text = new Text("Ik ken echt niemand die beter is dan Cedric #GrootEgo");
        System.out.println("*** Words containing e ***");
        text.printFilteredWords(s -> s.contains("e"));
        System.out.println("*** Long words ***");
        text.printFilteredWords(s -> s.length() > 4);
        System.out.println("*** Words starting with C ***");
        text.printFilteredWords(s -> s.startsWith("C"));

        //oef 4
        System.out.println("*** Words second letter e ***");
        text.printFilteredWords(s -> s.startsWith("e", 1));
        System.out.println("*** Words two letters e ***");
        text.printFilteredWords(s -> s.matches(".*e.*e.*"));
    }
}
