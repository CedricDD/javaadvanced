package oef4;

/**
 * Created by C�dric De Dycker on 11/10/2015.
 */
public interface WordFilter {
    public boolean isValid(String s);
}
