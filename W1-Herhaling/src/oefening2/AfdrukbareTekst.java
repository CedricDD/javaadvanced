package oefening2;

/**
 * Created by C�dric De Dycker on 23/09/2015.
 */
public class AfdrukbareTekst implements Afdrukbaar {

    private String code;
    private String inhoud;

    public AfdrukbareTekst(String code, String inhoud){
        this.code = code.replaceAll("\n"," ");
        this.inhoud = inhoud.replaceAll("\n"," ");
    }

    @Override
    public boolean equals(Object o){
    if(o instanceof AfdrukbareTekst && ((AfdrukbareTekst)o).code.equals(this.code)){
            return true;
    }else{
        return false;
    }
    }

    @Override
    public void print(int breedte) {
        int aantalLijnen = getAantallijnen(breedte);
        for (int i = 0; i < aantalLijnen; i++) {
            int beginpos = i*breedte;
            String tempString = "";
            if (i == aantalLijnen - 1) {
                tempString = inhoud.substring(beginpos);
            } else {
                tempString = inhoud.substring(beginpos,beginpos+breedte);
            }
            System.out.println(String.format("%d\t%s",i+1,tempString));
        }
        /*
        String temp = "";
        int teller = 0;
        int lijnen = 0;
        for(int i = 0; i<inhoud.length();i++){
            if(teller >=breedte){
                System.out.println((lijnen+1) +".\t" + temp);
                teller = 0;
                lijnen++;
                temp = "";
            }

            teller++;
            temp = temp + inhoud.substring(i,i+1);
            if(i == inhoud.length()-1){
                System.out.println((lijnen+1) +".\t" + temp);
            }

        }

        */

        /*
        int aantallijnen = this.getAantallijnen(breedte);
        String temp = formatString(breedte,aantallijnen);
        for(int i = 0; i < aantallijnen; i++){
        System.out.println((i + 1) + ".\t" + temp.substring((i * breedte), (i+1) *breedte));
        }
        */

    }

    @Override
    public int getAantallijnen(int breedte){
        /*
        int lijnen = 0;
        int karacters = inhoud.length();
        while(karacters >0){
            lijnen++;
            karacters -= breedte;

        }
        return lijnen;
        */
        return (int) Math.ceil((double) inhoud.length() / breedte);
    }

    //Zeer slechte code
    private String formatString(int breedte, int aantalLijnen){
        int maxLength = aantalLijnen * breedte;
        String temp = inhoud;
        for(int i = temp.length(); i < maxLength;i++){
            temp = temp + " ";
        }

        return temp;
    }

}
