package oefening2;

import java.util.ArrayList;

/**
 * Created by C�dric De Dycker on 23/09/2015.
 */
public class Artikel extends AbstractArtikel {

    private ArrayList<String> auteurs;

    public Artikel(String auteur, AfdrukbareTekst tekst) {
        super(tekst);
        auteurs = new ArrayList<String>();
        auteurs.add(auteur);
    }

    //Geen idee wat hiermee moet worden gedaan
    public int getAantalLijnen(int i) {
        return 0;
    }

    public void voegAuteurToe(String auteur) {
        auteurs.add(auteur);
    }

    @Override
    public void drukAf() {
        String temp = "";
        for (String s : auteurs) {
            temp = temp + s + "\t";
        }
        System.out.println(temp);
        super.drukAf();
    }
}
