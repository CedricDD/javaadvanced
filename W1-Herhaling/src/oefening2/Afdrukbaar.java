package oefening2;

/**
 * Created by C�dric De Dycker on 23/09/2015.
 */
public interface Afdrukbaar {

    void print(int i);
    int getAantallijnen(int i);
}
