package oefening2;

/**
 * Created by C�dric De Dycker on 23/09/2015.
 */
public class Test {
    public static void main(String[] args) {
        AfdrukbareTekst a = new AfdrukbareTekst("a","Nor Shapes of men Nor beasts we ken the ice was all\nBetween");

        Artikel aa = new Artikel("S. Colderidge", a);

        aa.voegAuteurToe("C. De Dycker");
        aa.voegAuteurToe("J. Jo");

        aa.drukAf();

    }
}
