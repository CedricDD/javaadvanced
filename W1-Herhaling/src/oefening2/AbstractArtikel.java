package oefening2;

/**
 * Created by C�dric De Dycker on 23/09/2015.
 */
public abstract class AbstractArtikel {

    private AfdrukbareTekst tekstInhoud;
    private static final int breedte = 20;

    public AbstractArtikel(AfdrukbareTekst tekst){
        this.tekstInhoud = tekst;
    }

    public void drukAf(){
        tekstInhoud.print(breedte);
    }

}
