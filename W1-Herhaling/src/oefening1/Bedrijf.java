package oefening1;

/**
 * Created by C�dric De Dycker on 23/09/2015.
 */
public interface Bedrijf {
    double getMaandLoon();
    void drukGegevens();
}
