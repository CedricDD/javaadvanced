package oefening1;

import java.lang.String;import java.lang.System; /**
 * Created by C�dric De Dycker on 23/09/2015.
 */
public abstract class Werknemer implements Bedrijf{
    private String naam, nr;
    private Datum datumInD;

    public Werknemer(String nr, String naam, Datum datum){
        this.nr = nr;
        this.naam = naam;
        this.datumInD = new Datum(datum);
    }

    public void drukWerknemer(){
        System.out.println("Werknemer: " + nr + " "+ naam+
        "\nDatum in dienst:"+datumInD.drukDatum());
    }
}
