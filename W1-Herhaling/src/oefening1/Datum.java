package oefening1;

import java.lang.String; /**
 * Created by C�dric De Dycker on 23/09/2015.
 */
public final class Datum {
    private int dag, maand, jaar;

    public Datum(int dag, int maand, int jaar){
        this.dag = dag;
        this.maand = maand;
        this.jaar = jaar;
    }

    public Datum(Datum copyDatum){
        this.dag = copyDatum.dag;
        this.maand = copyDatum.maand;
        this.jaar = copyDatum.jaar;
    }

    public String drukDatum(){
        return ""+dag+"/"+maand+"/"+jaar;
    }
}
