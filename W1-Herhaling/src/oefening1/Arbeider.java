package oefening1;

import java.lang.Override;import java.lang.String;import java.lang.System; /**
 * Created by C�dric De Dycker on 23/09/2015.
 */
public class Arbeider extends Werknemer{

    private double uurloon;
    private int aantalUren;

    public Arbeider(String nr, String naam, Datum datum, double loon, int aantalUren){
        super(nr,naam,datum);
        this.uurloon = loon;
        this.aantalUren = aantalUren;
    }

    @Override
    public double getMaandLoon() {
        if(aantalUren > 160){
            int overuren = aantalUren - 160;
            return (160 * uurloon + overuren * (uurloon * 1.5));
        }else{
            return uurloon * aantalUren;
        }
    }

    @Override
    public void drukGegevens() {
        super.drukWerknemer();
        System.out.println("Statuut: arbeider\nLoon: " + getMaandLoon() + " euro");
    }
}
