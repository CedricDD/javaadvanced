package oefening1;

import java.lang.Double;import java.lang.String;import java.lang.System;import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

/**
 * Created by C�dric De Dycker on 23/09/2015.
 */
public class Test {
    public static void main(String[] args) {
        Bediende b1 = new Bediende("B23", "Hannah Krekels", new Datum(1,2,2008), 3500.99);
        Bediende b2 = new Bediende("B52", "Veronique Verjans", new Datum(15,6,2000), 3499.99);

        Arbeider a1 = new Arbeider("A55", "Frankie Claessens", new Datum(1,1,1825),0.01,2500);
        Arbeider a2 = new Arbeider("A35", "Hans Habraken", new Datum(25,1,2013), 10.8, 130);

        ArrayList<Werknemer> gegevens = new ArrayList<Werknemer>();

        gegevens.add(b1);
        gegevens.add(b2);
        gegevens.add(a1);
        gegevens.add(a2);

        ArrayList<Double> lonen = new ArrayList<Double>();

        Iterator<Werknemer> i = gegevens.iterator();
        while(i.hasNext()){
            Werknemer w = i.next();
            w.drukGegevens();
            lonen.add(w.getMaandLoon());
        }

        double grootst = Double.MIN_VALUE;
        for (double d: lonen){
         if(d>grootst){
             grootst = d;
         }
        }
        System.out.println("Grootste loon: " + grootst);
    }
}
