package oefening1;

import java.lang.Override;import java.lang.String;import java.lang.System; /**
 * Created by C�dric De Dycker on 23/09/2015.
 */
public class Bediende extends Werknemer{
    private double loon;

    public Bediende(String nr, String naam, Datum datum, double loon){
        super(nr, naam, datum);
        this.loon = loon;
    }


    @Override
    public double getMaandLoon() {
        return loon;
    }

    @Override
    public void drukGegevens() {
        super.drukWerknemer();
        System.out.println("Statuut: bediende\nLoon: " + getMaandLoon() + " euro");
    }
}
