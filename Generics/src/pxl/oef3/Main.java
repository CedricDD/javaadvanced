package pxl.oef3;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by C�dric De Dycker on 1/10/2015.
 */
public class Main {

    public static void main(String[] args) {
        int a = 0;
        String[]array = {
                "Hallo","Ik","Ben","C�dric","0","2","22"
        };
        System.out.println(Arrays.toString(array));
        sortArray(array);
        System.out.println(Arrays.toString(array));

        System.exit(0);
    }

    private static <T> void swap(T[] array, int a, int b) {
        T temp = array[b];
        array[b] = array[a];
        array[a] = temp;
    }


    public static <T extends Comparable<T>> void sortArray(T[] array) {
        int i, j;
        int iMin;
        int n = array.length;
        for (j = 0; j < n - 1; j++) {
            iMin = j;
            for (i = j + 1; i < n; i++) {

                if (!(array[i].compareTo(array[iMin]) > 0)) {
                    iMin = i;
                }

            }

            if (iMin != j) {
                swap(array, j, iMin);
            }
        }

    }
}


