package pxl.oef4;

/**
 * Created by C�dric De Dycker on 1/10/2015.
 */
public class Pair {
    protected Object value1,value2;

    public Pair(Object value1, Object value2){
        this.value1=value1;
        this.value2=value2;
    }
    @Override
    public String toString(){
        return "("+value1.toString() + ","+value2.toString() + ")";
    }

}
