package pxl.oef4;

/**
 * Created by C�dric De Dycker on 1/10/2015.
 */
public class Optelling extends Pair {
    public Optelling(Object value1, Object value2) {
        super(value1, value2);
    }

    @Override
    public String toString(){
        return "("+super.value1.toString() + " + " +super.value2.toString() + ")";

    }
}
