package pxl.oef4;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * Created by C�dric De Dycker on 1/10/2015.
 */
public class Main {
    public static void main(String[] args) {
        Integer a = 5;
        String s = "Hallo";
        Pair pair1 = new Pair(a,s);
        Pair pair2 = new Pair(s,pair1);
        System.out.println("Pair1: "+pair1.toString());
        System.out.println("Pair2: "+pair2.toString());


        Pair pair3 = new Optelling(
                        new Optelling(new Pair(20,20),
                                                new Vermenigvuldiging(new Pair(2,-1),3))
                    ,   new Vermenigvuldiging(-1,
                                                new Pair(0,4)));

        System.out.println("Pair3: "+pair3.toString());

        try {
            solve(pair3.toString());
        } catch (ScriptException e) {
            e.printStackTrace();
        }

        System.exit(0);
    }

    /*
    Gebruikt javascript om de string als een bewerking op te lossen
     */
    public static void solve(String expression) throws ScriptException {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        try {
            System.out.println(engine.eval(expression));
        } catch (ScriptException e) {
            e.printStackTrace();
        }
    }
}
