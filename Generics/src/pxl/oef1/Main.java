package pxl.oef1;

import java.util.Arrays;

/**
 * Created by C�dric De Dycker on 30/09/2015.
 */
public class Main {

    public static void main(String[] args) {

        System.out.println("Integer");

        int[] getallen = {0,1,2,3,4,5,6};
        System.out.println(Arrays.toString(getallen));
        omkerenInt(getallen);
        System.out.println(Arrays.toString(getallen));

        System.out.println("String");

        String[] woorden = {"Ik", "ben","cool"};
        System.out.println(Arrays.toString(woorden));
        omkerenString(woorden);
        System.out.println(Arrays.toString(woorden));

        System.out.println("Generisch");
        System.out.println(Arrays.toString(getallen));
        omkerenInt(getallen);
        System.out.println(Arrays.toString(getallen));
        System.out.println(Arrays.toString(woorden));
        omkerenString(woorden);
        System.out.println(Arrays.toString(woorden));

        System.exit(0);

    }

    public static void omkerenInt(int[] array){
        //een echte copy maken ipv reference
        int[] copy = Arrays.copyOf(array, array.length);
        //Omdraaien
        for(int i = 0; i < array.length; i++){
            array[i] = copy[array.length-i -1];
        }
    }

    public static void omkerenString(String[] array){
        //een echte copy maken ipv reference
        String[] copy = Arrays.copyOf(array, array.length);
        //Omdraaien
        for(int i = 0; i < array.length; i++){
            array[i] = copy[array.length-i -1];
        }
    }

    /*
    Ook juist
    public static void omkerenArray(Object[] array){
        //een echte copy maken ipv reference
        Object[] copy = Arrays.copyOf(array, array.length);
        //Omdraaien
        for(int i = 0; i < array.length; i++){
            array[i] = copy[array.length-i -1];
        }
    }
     */
    public static <T>void omkerenArray(T[] array){
        //een echte copy maken ipv reference
        T[] copy = Arrays.copyOf(array, array.length);
        //Omdraaien
        for(int i = 0; i < array.length; i++){
            array[i] = copy[array.length-i -1];
        }
    }
}
