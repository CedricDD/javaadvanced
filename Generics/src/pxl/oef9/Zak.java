package pxl.oef9;

import pxl.oef6.MyList;

/**
 * Created by C�dric De Dycker on 3/10/2015.
 */
public class Zak {

    private MyList list;

    public Zak(){
        list = new MyList(Object.class);
    }

    public <E> void add(E element) {
        list.add(element);
    }

    public <T> Object getRandomElementOfType(T element) {
        MyList copy = new MyList(element.getClass());
        for (int i = 0; i < list.size(); i++) {
            if (element.getClass().isInstance(list.get(i))) {
                copy.add(list.get(i));
            }
    }
    return copy.get((int) (copy.size() * Math.random()));
    }
}
