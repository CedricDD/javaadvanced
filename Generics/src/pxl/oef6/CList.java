package pxl.oef6;

import java.lang.reflect.Constructor;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by C�dric De Dycker on 3/10/2015.
 */
public class CList extends AbstractList{

    private int c_size;
    private int c_limit;
    private Object[] c_array;

    public CList(){

    }


    private void makeSpace(int count){
        if(c_limit - c_size < count){
            Object[] copy;
            if(c_array == null){
                copy = new Object[count + 4];
            }else {
                copy = new Object[c_size * 2 + count];
                System.arraycopy(c_array, 0, copy, 0, c_size);
            }
            c_array = copy;
            c_limit = copy.length;
        }
    }

    public Object get(int index) {
        if (index >= 0 && index < c_size) {
            return c_array[index];
        } else {
            throw new IndexOutOfBoundsException("Index " + index + " is out of valid range 0-" + (c_size - 1));
        }
    }

    public int size() {
        return c_size;
    }

    public void add(int index, Object element) {
        if (index >= 0 && index <= c_size) {
            makeSpace(1);
            if (index < c_size) {
                System.arraycopy(c_array, index, c_array, index + 1, c_size - index);
            }
            c_array[index] = element;
            c_size++;
            modCount++;
        } else {
            throw new IndexOutOfBoundsException("Index " + index + " is out of valid range 0-" + c_size);
        }
    }

    public Object remove(int index) {
        if (index >= 0 && index < c_size) {
            Object item = c_array[index];
            int start = index + 1;
            System.arraycopy(c_array, start, c_array, index, c_size - start);
            c_array[--c_size] = null;
            modCount++;
            return item;
        } else {
            throw new IndexOutOfBoundsException("Index " + index + " is out of valid range 0-" + (c_size - 1));
        }
    }

    public String toString(){
        return Arrays.toString(c_array);
    }
}
