package pxl.oef6;

import java.util.AbstractList;
import java.util.Objects;

/**
 * Created by C�dric De Dycker on 5/10/2015.
 */
@SuppressWarnings("unused")
public class MyList<E> extends AbstractList{

    protected E[] m_array;
    protected int arraySize;
    protected int arrayLimit;
    private final Class<E> classType;

    public <T>MyList(Class<E> classType){
        /*
            No array will be created before actually adding element
            to save memory
         */
        this.classType = classType;
    }


    private void configureArray(Object[] array) throws ClassCastException{
        m_array = (E[]) array;
    }

    private void makeSpace(int count){
        if(arrayLimit - arraySize < count){
            Object[] copy;
            if(m_array == null){
                copy = new Object[count +4];
            }else{
                copy = new Object[arraySize *2 + count];
                System.arraycopy(m_array, 0, copy, 0,arraySize);
            }
            configureArray(copy);
            arrayLimit = copy.length;
        }else{
            /*
                No need to make array bigger
             */
        }

    }



    public void add(int index, Object element){
       /*
        If casting succeeds continue else throw exception
        */
        try{
            E object = classType.cast(element);
            if(index >= 0&& index <= arraySize){
                makeSpace(1);
                if(index < arraySize){
                    System.arraycopy(m_array, index, m_array, index + 1, arraySize - index);
                }
                m_array[index] =object;
                arraySize++;
                modCount++;
            }
        }catch(ClassCastException e){
            throw new ClassCastException("Element: " + e.getClass() + " does not match " + classType);
        }
    }

    public E remove(int index){
        if (index >= 0 && index < arraySize) {
            E item = m_array[index];
            int start = index + 1;
            System.arraycopy(m_array, start, m_array, index, arraySize - start);
            m_array[--arraySize] = null;
            modCount++;
            return item;
        } else {
            throw new IndexOutOfBoundsException("Index " + index + " is out of valid range 0-" + (arraySize - 1));
        }
    }


    public E get(int index){
        if(index >= 0 && index < arraySize){
            return m_array[index];
        }else{
            throw new IndexOutOfBoundsException("Index " + index +
                    " is out of valid range 0-" + (arraySize - 1));
        }
    }

    public int size(){return arraySize;}

    public String toString(){
        return java.util.Arrays.toString(m_array);
    }

    public Class<E> getClassType() {
        return classType;
    }

    public E[] getArray() {
        return m_array;
    }

    protected void setSize(int size){
        this.arraySize = size;
        this.arrayLimit = size;
    }

}
