package pxl.oef7;

/**
 * Created by C�dric De Dycker on 3/10/2015.
 */
public class Konijn extends Dier {
    public Konijn(String naam) {
        super(naam, DierType.zoogdier);
    }

    @Override
    public void beweeg() {
        System.out.println("Ik hop.");
    }

    @Override
    public void geluid() {
        System.out.println("(konijnen geluid)");
    }

    @Override
    public String toString(){
        return "Ik ben " + naam + " ,een "+ getClass().getSimpleName();
    }
}
