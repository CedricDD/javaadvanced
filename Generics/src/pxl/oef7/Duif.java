package pxl.oef7;

import com.sun.javafx.geom.Vec2d;

/**
 * Created by C�dric De Dycker on 3/10/2015.
 */
public class Duif extends Dier{



    public Duif(String naam) {
        super(naam, DierType.vogel);
    }

    @Override
    public void beweeg() {
        System.out.println("Ik vlieg.");
    }

    @Override
    public void geluid() {
        System.out.println("roekoe koe");
    }
    @Override
    public String toString(){
        return "Ik ben " + naam + " ,een "+ getClass().getSimpleName();
    }
}
