package pxl.oef7;

/**
 * Created by C�dric De Dycker on 3/10/2015.
 */
public class Main {

    public static void main(String[] args) {
        Hoed magischeHoed = new Hoed();
        magischeHoed.voegToe(new Duif("Jan"));
        magischeHoed.voegToe(new Konijn("Piet"));

        boolean temp =  false;
        while (!temp){
           temp = magischeHoed.magic();
        }

    }
}
