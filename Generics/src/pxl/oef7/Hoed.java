package pxl.oef7;

import pxl.oef6.MyList;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by C�dric De Dycker on 5/10/2015.
 */
public class Hoed{

    private MyList<Dier> inhoud;
    java.util.Random random = new Random();

    public Hoed(){
        inhoud = new MyList<Dier>(Dier.class);
    }

    public <T extends Dier>boolean voegToe(T element){
        return inhoud.add(element);
    }

    public boolean magic(){
        if(random.nextBoolean()){
            System.out.println(inhoud.get(random.nextInt(inhoud.size())).toString());
            return true;
        }else{
            System.out.println("Er is niets uit de hoed gekomen!");
            return false;
        }
    }
}
