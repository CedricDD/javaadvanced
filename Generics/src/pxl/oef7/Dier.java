package pxl.oef7;

/**
 * Created by C�dric De Dycker on 3/10/2015.
 */
public abstract class Dier {

    protected String naam;
    protected DierType type;


    public Dier(String naam, DierType dierType){
        this.naam = naam;
        this.type = dierType;
    }

    public abstract void beweeg();
    public abstract void geluid();

    public enum DierType{
        zoogdier,
        vis,
        vogel,
        reptiel
    }

    @Override
    public String toString(){
        return "Ik ben " + naam + " ,een "+ getClass().getSimpleName();
    }

}
