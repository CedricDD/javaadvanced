package pxl.oef2;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created by C�dric De Dycker on 30/09/2015.
 */
public class Main {

    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {

        System.out.println("Integer");

        int[] getallen = new int[5];
        System.out.println(Arrays.toString(getallen));
        vulOpInt(getallen, 5);
        System.out.println(Arrays.toString(getallen));

        System.out.println("Generisch");
        String[] woorden = new String[5];
        System.out.println(Arrays.toString(woorden));
        String s = "C�dric is cool";
        vulOp(woorden, s);
        System.out.println(Arrays.toString(woorden));
        woorden[2] = "Help";
        System.out.println(Arrays.toString(woorden));
        System.exit(0);
    }

    public static void vulOpInt(int[] array, int a){
        if(array.length ==0 || array==null)return;
        for(int i =0; i < array.length;i++){
            array[i] = a;
        }

    }

    public static <T> void vulOp(T[] array, T element){
        if(array.length ==0 || array==null)return;
        for(int i = 0; i < array.length; i++)array[i] = element;
    }



}
