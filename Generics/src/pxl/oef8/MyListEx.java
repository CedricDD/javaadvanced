package pxl.oef8;

import pxl.oef6.MyList;

import java.util.Objects;

/**
 * Created by C�dric De Dycker on 5/10/2015.
 */
public class MyListEx<T> extends pxl.oef6.MyList {


    public <T> MyListEx(Class classType) {
        super(classType);
    }

    public <T>MyListEx(MyListEx copy){
         super(copy.getClassType());
         m_array = java.util.Arrays.copyOf(copy.toArray(), copy.size());
        super.setSize(copy.size());

    }

    /*
        returns object array.
        user should now what the array consists of.
     */
    public <U extends Filter>MyListEx<U> filteredList(U element){
        MyListEx<U> list = new MyListEx<U>(this);
        for (int i = 0; i < list.size(); i++){
            if(!element.test(list.get(i))){list.remove(i);}
        }
        return list;
    }
}
