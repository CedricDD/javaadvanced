package pxl.oef8;

/**
 * Created by C�dric De Dycker on 3/10/2015.
 */
public interface Filter<E> {

    boolean test(E element);

}
