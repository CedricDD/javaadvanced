package pxl.oef8;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by C�dric De Dycker on 5/10/2015.
 */
public class Main {

    public static void main(String[] args) {
        MyListEx<Persoon> lijst = new MyListEx<Persoon>(Persoon.class);
        Random r = new Random();
        for(int i =0; i <10; i++){
            if(r.nextBoolean()){lijst.add(new Student());}else{lijst.add(new Leraar());}
        }
        MyListEx<Student> studenten = new MyListEx<Student>((lijst.filteredList(new Student())));
        System.out.println(studenten.size());
        System.out.println(Arrays.toString(studenten.getArray()));
    }

}
